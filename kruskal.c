#include <stdio.h>
#include <string.h>
#include <stdlib.h>

/*
	*-----------------------------------*
	* -------- LES STRUCTURES --------- *
	*-----------------------------------*
*/

/**
* Déclaration d'une structure ListeElement (List) correspendant au chemin d'un arbre (sommet1 - sommet2 - poid de l'arete)
*/
	typedef struct ListElement
	{
		char s1[100];
		char s2[100];
		int poid;
		struct ListElement * next;
	}List;

/**
* maliste contiendra la liste principal tel-que entrer par l'utilisateur au lancement du programe
* arbre_couvrant contiendra l'arbre couvrant min après le traitement 
* nbr_aretes contien le nombre d'aretes
* nbr_final nombre de chemins de l'arbre couvrant 
* poid final calcule du poid de l'arbre couvrant min
*/

	List *maliste = NULL;
	List *arbre_couvrant = NULL;

	int nbr_aretes = 0;
	int nbr_final =0;

	int poid_final = 0;

/**
* Déclaration d'une liste sommet contenant le nom des sommet
*/

	typedef struct Sommet
	{
		char nom[100];
		struct Sommet * next;
	}Sommet;
/**
* Initialisation d'un tableau ou chaque élement représente un liste (sommet) avec 'I' indice du tableau
*/
	Sommet *TableauListeSommets[70];
	int I = 0;

/*
	*-----------------------------------*
	* ----OPERATION SUR LES LISTES ---- *
	*-----------------------------------*
*/

/**
* Cette fonction cherche le min dans la liste contenant les chemins (aretes) 
*/

void * minChemin(){
	List *l1, *l2;
	float min;
	if(maliste == NULL){
		return NULL;
	}else{
		l2 = maliste;
		min = maliste->poid;
		for (l1 = maliste; l1 != NULL; l1=l1->next)
		{
			if(l1->poid < min){
				l2 = l1;
				min = l1->poid;
			}
		}
		return l2;
	}
}

/**
* Cette fonction retourne la position ou se trouve les sommets v1 et v2 de le tableau de liste smmet
*/

void positionDansTableau(char v1[100], char v2[100], int *i1, int *i2)
{

	Sommet *s;
	for(int i=0; i<I; i++){
		for(s=TableauListeSommets[i]; s!= NULL; s=s->next){
			if(strcmp(s->nom,v1) == 0){
				*i1 = i;
			}
			if(strcmp(s->nom,v2) == 0){
				*i2 = i;
			}
		}
	}
}

/**
* Cette fonction ajoute la liste ou se trouve le sommet2 dans la position i2 a la liste dont la position est i1
*/
void ajouteI2aI1(int i1, int i2){

	Sommet *s;
	for (s=TableauListeSommets[i1]; s->next!=NULL; s=s->next){
		
	}
	s->next = TableauListeSommets[i2];
	TableauListeSommets[i2] = NULL;

}

/**
* Cette fonction ajoute un element a la liste arbre_couvrant 
*/

void ajouter_a_arbre_couvrant(char v1[100], char v2[100], float poid){
	
	List *li = (List *) malloc (sizeof(List));
	li->next = NULL;

	strcpy(li->s1, v1);
	strcpy(li->s2, v2);
	li->poid = poid;
	List *l2;
	if(arbre_couvrant == NULL){
		arbre_couvrant = li;
	}else{
		for(l2=arbre_couvrant; l2->next; l2=l2->next);
			l2->next=li;
	}
	nbr_final++;
 	poid_final = poid_final + li->poid;
}

/**
* Cette fonction supprime un element de la liste 
*/
void sup_de_maliste(List *li){

	if(li == maliste){
		maliste = maliste->next;
		free(li);
	}else{
		List *l2;
		for(l2=maliste; l2->next != li; l2->next){
			l2 = l2->next;
		}
		l2->next = li->next;
		free(li);
	}
	nbr_aretes--;
}

// :::::::::::::: AFFICHER UNE LISTE ::::::::::::::::::::::::::::::::

void affichage_systeme()
{
	if(nbr_final == 0){
	  	printf("\n ::::::::::::::::::: PREMIERE OCCURENCE DU PROGRAME ::::::::::::::::::::: \n\n");
	}else{
	  	printf("\n\n ::::::::::::::::::: NOUVELLE OCCURENCE DU PROGRAME ::::::::::::::::::::: \n\n");
	}

 	printf("\n Représentation de la liste initial contenant '%d' chemins:\n", nbr_aretes);
  	List *li;
	for(li = maliste; li!=NULL; li=li->next){
		printf("%s -- %d -- %s\n",li->s1, li->poid, li->s2 );
	}
	printf("\n Représentation de la liste des sommet avec leurs indice dans le tableau:\n");
	Sommet *s;
	for(int i=0; i<I; i++){
		printf("\n{%d}",i);
		for(s = TableauListeSommets[i]; s!=NULL; s = s->next){
			printf(" -- [%s] ",s->nom );
		}
	}
	printf("\n\n Représentation de l'arbre couvrant contenant '%d' chemins:\n\n", nbr_final);
	for(li = arbre_couvrant; li!=NULL; li=li->next){
		printf("%s -- %d -- %s \n",li->s1,li->poid,li->s2 );
	}
}

/**
* Cette fonction va concstruire le tableau de liste sommet
* Ici chaque element du tableau contiendra une liste avec un element représentant un sommet 
* de manière a ce que tout les sommet de l'arbre y soit stocké
*/

void creer_tableau_liste_sommet(){

	Sommet *s;
	List *li;
	int b;

	// Allocation pour le sommet s
	s = (Sommet *) malloc (sizeof(Sommet));
	s->next=NULL;

	// On met le premier sommet (s1) de notre liste dans la liste sommet s
	strcpy(s->nom, maliste->s1);
	TableauListeSommets[I] = s;
	I++;

	// Continuer avcec les autre elements 
	for(li=maliste; li!= NULL; li=li->next){

		b=1; // Variable de condition (existance de l'element (sommet))

		// Verifier si l'element existe dans la case du tableau, s'il existe remmettre le b == 0 pour ne pas qu'il l'ajoute
		for(int i=0; i<I; i++){
			if(strcmp(li->s1, TableauListeSommets[i] -> nom) == 0){
				b=0;
			} 
		}

		// Si l'element n'existe pas créer une nouvelle liste sommet s et mettre s dans l'element du tableau i;)
		if(b==1){
			s = (Sommet *) malloc (sizeof(Sommet));
			s->next=NULL;
			strcpy(s->nom, li->s1);
			TableauListeSommets[I] = s;
			I++;
		}

		b=1;// deuxieme initialisation de b pour refaire la meme chose avec le 2em sommet du chemin / compliquer non ! ;)
		for (int i = 0; i < I; i++){
			if (strcmp(li->s2, TableauListeSommets[i]->nom) == 0){
				b=0;
			}
		}

		if(b==1){
			s = (Sommet *) malloc (sizeof(Sommet));
			s->next=NULL;
			strcpy(s->nom, li->s2);
			TableauListeSommets[I] = s;
			I++;
		}
	}
}

/**
* Cette fonction permet de romplir la liste maliste avec les element entrer par l'utilisateurs
*/

void ajouter_un_chemin(char som1[100], char som2[100], float poid)
{
	List *l1, *l2;
	l1 = (List *) malloc (sizeof(List));
	l1 -> next = NULL;
	strcpy(l1->s1, som1);
	strcpy(l1->s2, som2);
	l1->poid = poid;

	if(maliste == NULL){
		maliste = l1;
	}else{
		for(l2 = maliste; l2->next != NULL; l2 = l2->next);
			l2->next = l1;
	}
}


/*
	*-----------------------------------*
	* -- OPERATION SUR LES FICHIERS --- *
	*-----------------------------------*
*/

/**
* Cette fonction écrit dans un fichier 'arbreSortie.txt' l'arbre couvrant résultant du déroulement de cet algorithme 
*/

void arbreSortie(){
	FILE *fichierS;
	fichierS = fopen("arbreSortie.txt", "w");
	List *li;
	for(li=arbre_couvrant; li!=NULL; li=li->next){
		fprintf(fichierS, "%s %s %d\n",li->s1, li->s2, li->poid);

	}
	fclose(fichierS);
}


/**
* Cette fonction lit un fichier arbreEntree.txt et fait appel a une fonction qui remplis les element d'une liste
*/

void Lire_fichier(int nbr_arretes){

	FILE *fichier = fopen("arbreEntree.txt", "r");

	if(fichier == NULL){
		exit(1);
	}

	char sommet1[100], sommet2[100];

	int poid;
		
	//printf("Lire_fichier // L'arbre que l'on doit minimaliser est le suivant\n\n");

	for (int i = 0; i < nbr_arretes; ++i){
		fscanf(fichier, "%s %s %d", sommet1, sommet2, &poid);
		//printf("%s -- %d -- %s \n",sommet1, poid, sommet2);
		ajouter_un_chemin(sommet1, sommet2, poid);
	}

	fclose(fichier);

}

/**
* Cette fonction ne sère qu'a enlever le saut de ligne avant d'écrire dans le fichier
*/

int lire(char *chaine, int longueur)
{
    char *positionEntree = NULL;
 
    // On lit le texte saisi au clavier
    if (fgets(chaine, longueur, stdin) != NULL)  // Pas d'erreur de saisie ?
    {
        positionEntree = strchr(chaine, '\n'); // On recherche l'"Entrée"
        if (positionEntree != NULL) // Si on a trouvé le retour à la ligne
        {
            *positionEntree = '\0'; // On remplace ce caractère par \0
        }
        return 1; // On renvoie 1 si la fonction s'est déroulée sans erreur
    }
    else
    {
        return 0; // On renvoie 0 s'il y a eu une erreur
    }
}

/**
* Ecrit dans un fichier l'arbre que l'utilisateur fourni
*/

int Ecrir_fichier(){

	FILE *fichier = fopen("arbreEntree.txt", "w");

	int nbr_arretes = 0;
	char text[255] = "Bonjour les gens";

	printf("Veuillez indiquez le nombre d'arretes de votre graphe \n");
	scanf("%d", &nbr_arretes);

	printf("veillez entrez sous forme de lignes, les sommets associer avec le poid des arretes qui les lies\n");
	
	lire(text, 255);
	
	for (int i = 0; i < nbr_arretes; i++)
	{
		fgets(text, 255, stdin);
		fputs(text, fichier);
	}

	fclose(fichier);
	
	return nbr_arretes;

}

/*
	*-----------------------------------*
	* ------- FONCTION KRUSKAL -------- *
	*-----------------------------------*
*/

/**
* Cette fonction est une fonction récursive, c'est le coeur du programe 
*/
void kruskal(){

	Sommet *s;
	affichage_systeme();

	// remplir le fichier une fois tout terminer
	if(maliste == NULL){
		arbreSortie();
		return;
	}

	// Le chemin avec la plus petite arete
	List * min = minChemin(); // On est obliger sinon il faudra faire une allocation memoir et dupliquer
	//printf("\n le plus court chemin est (%s -- %d -- %s)\n", min->s1, min->poid, min->s2);

	int i1,i2;

	positionDansTableau(min->s1, min->s2, &i1, &i2);
	//printf("la position de s1 est = %d et s2 = %d\n", i1, i2);
	
	if(i2 != i1){
		ajouteI2aI1(i1,i2);// attache la liste i2 a la liste i1
		ajouter_a_arbre_couvrant(min->s1, min->s2, min->poid);

		// SUpprimer le chemin de l'arbre
		sup_de_maliste(min);
	}else{
		sup_de_maliste(min);
	}

	kruskal();

}

/*
	*-----------------------------------*
	* -------- FONCTION MAIN ---------- *
	*-----------------------------------*
*/

int main()
{
	
	printf("\n----------------- ALGORITHME DE KRUSKAL ----------------\n\n");	

	// Ecrir l'arbre dans le fichier
	nbr_aretes = Ecrir_fichier();

	// Lire le fichier et construire une liste
	Lire_fichier(nbr_aretes);

	// Très important :: Créer le tableau contenant les liste de sommets pour l'algoritme de kruskal avec chaque sommet comme début d'un liste
	creer_tableau_liste_sommet();

	kruskal();

	printf("\n\n ::::::::::::::: LE POID DE L'ARBRE COUVRANT EST [%d] ::::::::::::::::::\n\n", poid_final );

	return 0;
}