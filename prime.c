#include <stdio.h>
#include <string.h>
#include <stdlib.h>

/*
  *-----------------------------------*
  * -------- LES STRUCTURES --------- *
  *-----------------------------------*
*/

/**
* Déclaration d'une structure ListeElement (List) correspendant au chemin d'un arbre (sommet1 - sommet2 - poid de l'arete)
*/
  typedef struct ListElement
  {
    char s1[100];
    char s2[100];
    int poid;
    struct ListElement * next;
  }List;

/**
* maliste contiendra la liste principal tel-que entrer par l'utilisateur au lancement du programe
* arbre_couvrant contiendra l'arbre couvrant min après le traitement 
* nbr_aretes contien le nombre d'aretes
* nbr_final nombre de chemins de l'arbre couvrant 
* poid final calcule du poid de l'arbre couvrant min
*/

  List *maliste = NULL;
  List *arbre_couvrant = NULL;
 // List *chemin_temp = NULL;

  int nbr_aretes = 0;
  int nbr_final =0;

  int poid_final = 0;

/**
* Déclaration d'une liste sommet contenant le nom des sommet
*/
  typedef struct Sommet
  {
    char nom[100];
    struct Sommet * next;
  }Sommet;

/**
* initialisation de la liste sommet a Null
*  déclaration d'une variable nbr_sommet 
*/
  Sommet *sommet = NULL;
  int nbr_sommet = 0;


/*
  *-----------------------------------*
  * ------- FONCTION AFFICHAGE ------ *
  *-----------------------------------*
*/

/**
* Cette fonction ne sère qu'a afficher l'état du système 
* (le contenu des listes d'aretes et de sommets)
* la construction de l'arbre couvrant en focntion de la récursive de prime()
*/

void affichage_systeme()
{

  if(nbr_final == 0){
    printf("\n ::::::::::::::::::: PREMIERE OCCURENCE DU PROGRAME ::::::::::::::::::::: \n\n");
  }else{
    printf("\n\n ::::::::::::::::::: NOUVELLE OCCURENCE DU PROGRAME ::::::::::::::::::::: \n\n");
  }


  printf("\n Représentation de la liste initial contenant '%d' chemins:\n", nbr_aretes);
  List *li;
  for(li = maliste; li!=NULL; li=li->next){
    printf("%s -- %d -- %s\n",li->s1, li->poid, li->s2 );
  }
  printf("\n Représentation de la liste des sommets contenant '%d' sommets:\n", nbr_sommet);
  
  Sommet *s;
  for(s=sommet; s!=NULL; s=s->next){
    printf(" [%s] ",s->nom);
  }

  printf("\n Représentation de l'arbre couvrant contenant '%d' chemins:\n", nbr_final);
  for(li = arbre_couvrant; li!=NULL; li=li->next){
    printf("%s -- %d -- %s \n",li->s1,li->poid ,li->s2);
  }
}

/*
  *-----------------------------------*
  * ----OPERATION SUR LES LISTES ---- *
  *-----------------------------------*
*/

/**
* Cette fonction cherche le min dans la liste contenant les chemins (aretes) 
* le min oui mais pas dans toute la liste 
* elle cherche en fonction des sommet enregistrer pour respecter le principe de prime
*/

void * minChemin(){
  List *l1, *l2;
  Sommet *s;

  int min;
  if(maliste == NULL){
    return NULL;
  }else{
    l2 = maliste;
    min = maliste->poid;
    for (l1 = maliste; l1 != NULL; l1=l1->next)
    {
      int verif =0;
      for (s = sommet; s!=NULL; s=s->next)
      {
        if(strcmp(s->nom, l1->s1) == 0){
          verif++;
        }
        if (strcmp(s->nom, l1->s2) == 0)
        {
          verif++;
        }
        if(verif != 0){
          if(l1->poid < min){
            l2 = l1;
            min = l1->poid;
          }
        }
      
      }
     
    }
    return l2;
  }
}


/**
* Cette fonction comme son nom l'indique ajoute des elements a la liste sommet
* avant d'ajouter un sommet il faut s'assurer que ce dernier n'existe pas déja dans la liste
* cette fonction retourne un int indiquant si le sommet poura étre ajouter a l'arbre couvrant ou pas
*/
int ajouter_sommet(char x1[100], char x2[100])
{
  int verif = 0;
  int v1 = 0; int v2 = 0;
  Sommet *s, *temp;
  s = (Sommet *) malloc (sizeof(Sommet));

  for (temp = sommet; temp!= NULL; temp=temp->next)
  {
    if(strcmp(temp->nom, x1) == 0){
      v1 = 1;
    }
    if(strcmp(temp->nom, x2) == 0){
      v2 = 1;
    }
  }

  if(v1 == 0){
    strcpy(s->nom, x1);
    s->next = sommet;
    verif = 1;
  }
  if(v2 == 0){
    strcpy(s->nom, x2);
    s->next = sommet;
    verif = 1;
  }
  if(v1 == 0 || v2 == 0){
    sommet = s; 
    nbr_sommet++;
  }
  
  return verif;
}

/**
* Cette fonction ajoute un element a la liste arbre_couvrant 
*/
void ajouter_a_arbre_couvrant(char v1[100], char v2[100], float poid){
  
  List *li = (List *) malloc (sizeof(List));
  li->next = NULL;

  strcpy(li->s1, v1);
  strcpy(li->s2, v2);
  li->poid = poid;
  List *l2;
  if(arbre_couvrant == NULL){
    arbre_couvrant = li;
  }else{
    for(l2=arbre_couvrant; l2->next; l2=l2->next);
      l2->next=li;
  }
  nbr_final++;
  poid_final = poid_final + li->poid;
}


/**
* Cette fonction sert a ajouter un première element a la liste sommet afin de commencer le traitement en réspéctant le principe de l'algorithme de PRIME
*/
void remplir_sommet()
{
  List *li;
  li = (List *) malloc (sizeof(List));

  Sommet *s;
  s = (Sommet *) malloc (sizeof(Sommet));

  li = maliste;

  if(sommet == NULL){

    s->next = NULL;
    strcpy(s->nom, li->s1);
    sommet = s;

    nbr_sommet++;
  }
}

/**
* Cette fonction permet de romplir la liste maliste avec les element entrer par l'utilisateurs
*/

void ajouter_un_chemin(char som1[100], char som2[100], float poid)
{
  List *l1, *l2;
  l1 = (List *) malloc (sizeof(List));
  l1 -> next = NULL;
  strcpy(l1->s1, som1);
  strcpy(l1->s2, som2);
  l1->poid = poid;

  if(maliste == NULL){
    maliste = l1;
  }else{
    for(l2 = maliste; l2->next != NULL; l2 = l2->next);
      l2->next = l1;
  }
}

/**
* Cette fonction supprime un element de la liste 
*/
void sup_de_maliste(List *li){
  if(li == maliste){
    maliste = maliste->next;
    free(li);
    nbr_aretes--;
  }else{
    List *l2;
    for(l2=maliste; l2->next != li; l2->next){
      l2 = l2->next;
    }
    l2->next = li->next;
    free(li);
    nbr_aretes--;
  }
}

/*
  *-----------------------------------*
  * -- OPERATION SUR LES FICHIERS --- *
  *-----------------------------------*
*/

/**
* Cette fonction écrit dans un fichier 'arbreSortie.txt' l'arbre couvrant résultant du déroulement de cet algorithme 
*/

void arbreSortie(){
  FILE *fichierS;
  fichierS = fopen("arbreSortie.txt", "w");
  List *li;
  for(li=arbre_couvrant; li!=NULL; li=li->next){
    fprintf(fichierS, "%s %s %d\n",li->s1, li->s2, li->poid);

  }
  fclose(fichierS);
}

/**
* Cette fonction lit un fichier arbreEntree.txt et fait appel a une fonction qui remplis les element d'une liste
*/

void Lire_fichier(int nbr_arretes){

  FILE *fichier = fopen("arbreEntree.txt", "r");

  if(fichier == NULL){
    exit(1);
  }

  char sommet1[100], sommet2[100];

  int poid;
    
  for (int i = 0; i < nbr_arretes; ++i){
    fscanf(fichier, "%s %s %d", sommet1, sommet2, &poid);
    //printf("%s -- %d -- %s \n",sommet1, poid, sommet2);
    ajouter_un_chemin(sommet1, sommet2, poid);
  }

  fclose(fichier);

}

/**
* Cette fonction ne sère qu'a enlever le saut de ligne avant d'écrire dans le fichier
*/

int lire(char *chaine, int longueur)
{
    char *positionEntree = NULL;
 
    // On lit le texte saisi au clavier
    if (fgets(chaine, longueur, stdin) != NULL)  // Pas d'erreur de saisie ?
    {
        positionEntree = strchr(chaine, '\n'); // On recherche l'"Entrée"
        if (positionEntree != NULL) // Si on a trouvé le retour à la ligne
        {
            *positionEntree = '\0'; // On remplace ce caractère par \0
        }
        return 1; // On renvoie 1 si la fonction s'est déroulée sans erreur
    }
    else
    {
        return 0; // On renvoie 0 s'il y a eu une erreur
    }
}

/**
* Ecrit dans un fichier l'arbre que l'utilisateur fourni
*/

int Ecrir_fichier(){

  FILE *fichier = fopen("arbreEntree.txt", "w");

  int nbr_arretes = 0;
  char text[255] = "Bonjour les gens";

  printf("Veuillez indiquez le nombre d'arretes de votre graphe \n");
  scanf("%d", &nbr_arretes);

  printf("veillez entrez sous forme de lignes, les sommets associer avec le poid des arretes qui les lies\n");
  
  lire(text, 255);
  
  for (int i = 0; i < nbr_arretes; i++)
  {
    fgets(text, 255, stdin);
    fputs(text, fichier);
  }

  fclose(fichier);
  
  return nbr_arretes;

}


/*
  *-----------------------------------*
  * -------- FONCTION PRIME --------- *
  *-----------------------------------*
*/


/**
* Cette fonction est une fonction récursive, c'est le coeur du programe 
*/
void prime()
{
  
  affichage_systeme();
  // remplir le fichier une fois tout terminer
  if(maliste == NULL){
    arbreSortie();
    return;
  }

  List * min = minChemin();
  printf("ICI MIN %s--%d--%s\n", min->s1, min->poid, min->s2);

  int verif = ajouter_sommet(min->s1, min->s2);

  if(verif == 1){
    ajouter_a_arbre_couvrant(min->s1, min->s2, min->poid);
    sup_de_maliste(min);
  }else{
    sup_de_maliste(min);
  }

  prime();
}

/*
  *-----------------------------------*
  * -------- FONCTION MAIN ---------- *
  *-----------------------------------*
*/

int main()
{
  
  printf("\n----------------- ALGORITHME DE PRIME ----------------\n\n"); 

  // Ecrir l'arbre dans le fichier
  nbr_aretes = Ecrir_fichier();
 
  // Lire le fichier et construire une liste
  Lire_fichier(nbr_aretes);

  remplir_sommet();

  prime();

  printf("\n\n ::::::::::::::: LE POID DE L'ARBRE COUVRANT EST [%d] ::::::::::::::::::\n\n", poid_final );

  return 0;
}